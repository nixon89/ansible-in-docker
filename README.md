# ansible-in-docker

Dockerized ansible 2.8.0 with Alpine 3.10 as base image. (~181mb image size)

For build image use:
```
docker build -t my-dockerized-ansible .
```

For check ansible version use:
```
docker run --rm -it my-dockerized-ansible ansible --version
```

Output:
```
ansible 2.8.0
  config file = None
  configured module search path = [u'/ansible/library']
  ansible python module location = /usr/lib/python2.7/site-packages/ansible
  executable location = /usr/bin/ansible
  python version = 2.7.16 (default, May  6 2019, 19:28:45) [GCC 8.3.0]


```


Image size:
```
$ docker images
REPOSITORY              TAG      IMAGE ID      CREATED        SIZE
my-dockerized-ansible   latest   e0ddb466c715  5 minutes ago  181MB
```